

// ncpu for bogo., example:   ncpu /proc/cpuinfo     
// it will display the list of bogomips values in one line.


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>


static int cat_fd(int fd) 
{
  char buf[4096];
  ssize_t nread;
  char ptr[4096];
  int i,j=0;

  while (( nread = read( fd, buf, sizeof buf)) > 0) 
  {
    ssize_t ntotalwritten = 0;
    while ( ntotalwritten < nread) 
    {

      ssize_t nwritten = sizeof( buf );

      for( i = 0;  i <= sizeof( buf ) ; i++)
      {
        if ( buf[i] == '\n' ) 
	{
          ptr[j]='\0';
          if ( strstr( ptr , "bogo" ) != 0 )  
             printf( "%s" , ptr );
	  j = 0 ; 
	}
        else
	{
          ptr[j++]=buf[i];
	}
      } 

      if ( nwritten < 1 )
        return -1;

      ntotalwritten += nwritten;
    }
  }
  return nread == 0 ? 0 : -1;
}









static int minicat(const char *fname) 
{
  int fd, success;
  if ((fd = open(fname, O_RDONLY)) == -1)
    return -1;

  success = cat_fd(fd);

  if (close(fd) != 0)
    return -1;

  return success;
}







int main( int argc, char *argv[])
{
      int i; 
      if ( argc >= 2)
      {
         for( i = 1 ; i <= argc-1 ; i++) 
            minicat( argv[ i ] );
      }
      return 0;
}






